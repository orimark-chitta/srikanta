import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';

import {database} from './database/database.js';
import userRoutes from './routes/users.js';
import glossaryTermRoutes from './routes/GlossaryTerm.js';
import errorHandler from './middleware/errorHandler.js';
const PORT = process.env.PORT || 3024;

const app = express();
dotenv.config();
database();

//middlewares
app.use(express.json());
app.use(cors());

app.use('/users',userRoutes);
app.use('/glossaryTerms',glossaryTermRoutes);
app.use(errorHandler);

app.listen(PORT,()=>{
    console.log('App is running on port',PORT)
})
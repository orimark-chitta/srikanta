import mongoose from "mongoose";

const GlossaryTermSchema = mongoose.Schema({
    term : {
        type : String,
        required : true
    },
    definition : {
        type : String,
        required : true
    }
},{
    timestamps:true
})

const GlossaryTerm = mongoose.model('GlossaryTerm',GlossaryTermSchema);
export default GlossaryTerm;
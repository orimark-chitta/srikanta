import jwt from "jsonwebtoken";
import bcrypt from 'bcryptjs';

import User from "../models/users.js";

export const createUser = async (req, res, next) => {

  const UserExists = await User.findOne({ userName: req.body.userName })

  if (UserExists) {
    //   res.status(400)
    res.status(400).json({ message: 'User already exists' });
  }

  try {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    const user = await User.create({
      ...req.body,
      password: hashedPassword
    })
    if (user) {
      res.status(201).json({
        user,
        message: 'User successfully created.'
      })
    } else {
      res.status(400).json({
        message: 'Invalid data'
      })
    }

  } catch (error) {
    console.log(error);
    next(error);
  }
}

export const deleteUser = async (req, res) => {
  if (!req.body._id) {
    return res.status(400).json({
      code: 400,
      message: 'Invalid _id!'
    })
  }
  try {

    const deletedUser = await User.findByIdAndDelete({ _id: req.body._id }, { new: true })
    if (deletedUser) {
      return res.status(200).json({
        message: "User deleted successfully",
        deletedUser,
        code: 200,
      })
    };
    return res.status(200).json({
      message: "No User Found!!",
      code: 404,
    })
  } catch (error) {
    console.log(error)
  }
}

export const getUsers = async (req, res) => {
  const users = await User.find();

  if (users.length > 0) {
    res.status(200).json({
      users,
      message: 'Users successfully fetched.'
    })
  } else {
    res.status(400).json({
      message: 'No data found!'
    })

  }
}

export const loginUser = async (req, res) => {

  try {
    const user = await User.findOne({ email: req.validatedBody.email });
  
    if (!user) {
      return res.status(400).json({
        message: 'Wrong Password or Email',
        code: 400
      })
    }
    const validPassword = await bcrypt.compare(req.validatedBody.password, user.password);
    
    const userData = { user };

    const { __v, password, ...data } = userData.user._doc;

    if (!validPassword) {
      return res.status(400).json({
        message: 'Wrong Password or Email',
        code: 400
      })
    }
    else {
      const token = await jwt.sign(data, process.env.TOKEN_SECRET, { expiresIn: "2d" });

      return res.json({
        message: "Login Successfull",
        code : 200,
        data,
        token
      })

    }
  } catch (error) {
      return res.status(400).json({
        message : 'Invalid Email Id or Password!',
        code : 400,
        err : error.message
      })
  }

}
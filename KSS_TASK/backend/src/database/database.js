import mongoose from 'mongoose';

export const database = async () => {
    try {
        mongoose.set('strictQuery', true);
        const db = await mongoose.connect(process.env.MONGO_URL);
        console.log('DB connected successfully.')
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
}
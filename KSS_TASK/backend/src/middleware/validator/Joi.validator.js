import Joi from '@hapi/joi';
import JoiObject from 'joi-objectid';
Joi.objectId = JoiObject(Joi);
// Joi.objectId = require('joi-objectid')(Joi)


export const getUserValidator = (req, res, next) => {
  const schema = Joi.object({
    api_key: Joi.string().trim().required(),
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    next({
      message : error.message,
      status : 401
    });
  } else {

    req.validatedBody = value;
    next();
  }
};

export const userLoginValidator = (req, res, next) => {
  const schema = Joi.object({
    // api_key: Joi.string().trim().required(),
    email : Joi.string().trim().email().trim(true).required(),
    password: Joi.string().trim().required(),
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    next({
      message : error.message,
      status : 401
    });
  } else {

    req.validatedBody = value;
    next();
  }
};

export const createTermValidator = (req, res, next) => {
  const schema = Joi.object({
    term: Joi.string().trim().required(),
    definition : Joi.string().trim().required(),
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    next({
      message : error.message,
      status : 400
    });
  } else {

    req.validatedBody = value;
    next();
  }
};
export const updateTermValidator = (req, res, next) => {
  const schema = Joi.object({
    term: Joi.string().trim(),
    definition : Joi.string().trim(),
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    next({
      message : error.message,
      status : 400
    });
  } else {

    req.validatedBody = value;
    next();
  }
};

export const createUserValidator = (req, res, next) => {
  const schema = Joi.object({
    userName: Joi.string().trim().required(),
    email : Joi.string().trim().email().trim(true).required(),
    role: Joi.string().trim().required(),
    password: Joi.string().trim().required(),
  });
  const { error, value } = schema.validate(req.body);
  if (error) {
    next({
      message : error.message,
      status : 400
    });
  } else {

    req.validatedBody = value;
    next();
  }
};
import jwt from "jsonwebtoken";

export const authUser = async (req, res, next) => {
    // if (!req.validatedBody.api_key || req.validatedBody.api_key !== process.env.API_KEY) {
    //     return next({
    //         message : "Invalid Api_key",
    //         status : 400
    //     })
    // }
    let bearerToken = req.header('Authorization');

    if (!bearerToken || bearerToken?.length < 30) {
        return res.json({
            status: 'failed',
            code: 401,
            message: 'No token provided'
          });
    }
    else {
        bearerToken = bearerToken.split(' ')[1];
        try {
            const userData = await jwt.verify(bearerToken, process.env.TOKEN_SECRET);
            req.user = userData;
            req.token = bearerToken;
            next();
        } catch (error) {
            res.json({
                status: 'failed',
                code: 401,
                err : error.message,
                error: 'You are not authorized to access this resource'
              });
        }
        
    }
    
}
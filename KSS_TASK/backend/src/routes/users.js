import express  from 'express';

import { createUser,  getUsers, loginUser, deleteUser } from '../controllers/users.js';
import { authUser } from '../middleware/auth.middleware.js';
import { createUserValidator, getUserValidator, userLoginValidator } from '../middleware/validator/Joi.validator.js';

const router = express.Router();

router.post('/createUser', createUserValidator, createUser);
// router.post('/updateUser', updateUser);
router.post('/deleteUser', deleteUser);
router.post('/getUsers', getUserValidator , getUsers);
router.post('/login', userLoginValidator , loginUser);

export default router;
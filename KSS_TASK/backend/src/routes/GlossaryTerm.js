import express  from 'express';
import { createGlossaryTerm, deleteGlossaryTerm, getTerms, updateGlossaryTerm, getSortedTerms } from '../controllers/GlossaryTerm.js';

import { authUser } from '../middleware/auth.middleware.js';
import { createTermValidator, updateTermValidator } from '../middleware/validator/Joi.validator.js';

const router = express.Router();

router.post('/createTerm', createTermValidator, authUser, createGlossaryTerm);
router.patch('/updateTerm/:_id', updateTermValidator, authUser, updateGlossaryTerm);
router.delete('/deleteTerm/:_id',authUser, deleteGlossaryTerm);
router.get('/getTerm', authUser, getTerms);
router.get('/getTermById/:_id', authUser, getTerms);
router.post('/sortAlphabatically', authUser, getSortedTerms);

export default router;